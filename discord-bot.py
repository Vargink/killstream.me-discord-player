# Killstream.me Discord bot.
#       Made by Vargink get the latest from https://bitbucket.org/Vargink/killstream.me-discord-player
# 
import discord
import urllib.request
import pickle
import asyncio
print("""                                                                                                    
                                            ./`                                                 
                    `..`    ``.`      `..``-yh.                                                 
                    ./so/-`  `:sho.    .+hdhyydmo`                                                 
                `.-/hmd-   .+hmmmo.``./hmmmmmmmho+++++//::-.````````````````````                   
            `.:oyhmmmmms//ohmmmmmmhsydmmmmmmmmmmmmmmmmmmmmmdhyyyhhhhhhhhhhhhhhyyso+:-.`            
        `:sdmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmdddmmmmmmmmmmmmmmmmmmhs/-`         
        `+hmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmddmmmmmmmmmmmmmmmmmmmdy/`       
    `:ymmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmddmmmmmmmmmmmmmmmmmmmmy-`     
    `ommmmmo////+++++++++++++++++++++++++++++////////////+shmmmmmmmmmdddddddddhhdmmmmmmmmmmd:     
    .smmmmmd:                                               .ommmmmmmmmo`````````.:smmmmmmmmmh.    
`smmmmmmd:                                                `ymmmmmmmmy`          `ommmmmmmmm/    
+mmmmmmmd:                      ``         ``             `ymmmmmmmmy`           -dmmmmmmmm+    
`hmmmmmmmmhhhhhhhhhhhhh/       `shhhhhhhhhhhhs`           .smmmmmmmmm+           `ommmmmmmmd:    
:mmmmmmmmmmmmmmmmmmmmmm+       `ymmmmmmmmmmmmd-     :ooooydmmmmmmmmmh.    `..`../ymmmmmmmmmy`    
+mmmmmmmmmmmmmmmmmmmmmm+       `ymmmmmmmmmmmmm:     `/hmmmmmmmmmmmmy.     `+hddmmmmmmmmmmmh-     
+mmmmmmmmmmmmmmmmmmmmmm+       `ymmmmmmmmmmmmds+:-`   `/hmmmmmmmmdo.``      .odmmmmmmmmmms.      
:mmmmmmmmmmmmmmmmmmmmmm+       `ymmmmmmmmmmmmddmmmds/.  `/shdddddddddhys+-`  `-odmmmmmdy:`       
`hmmmmmmmmmmmmmmmmmmmmm+       `ymmmmmmmmmmmmhmmmmmmmho-`  ```-odmmmmmmmmmdo-` `.:/+/:.`         
+mmmmmmmmmmmmmmmmmmmmm+       `ymmmmmmmmmmmdymmmmmmmmmds:`    `.+hmmmmmmmmmmy/`                 
`smmmmmmmmmmmmmmmmmmmm+       `ymmmmmmmmmmmo./ymmmmmmmmmmy/.     ./ymmmmmmmmmmh+.               
    .smmmmmmmmmmmmmmmmmmm+       `ymmmmmmmmmmo`  `:sdmmmmmmmmmho-`    `:sdmmmmmmmmmdo-`            
    `ommmmmmmmmmmmmmmmmm+       `ymmmmmmmmd+`     `-ohmmmmmmmmmds:`    `.+hmmmmmmmmmdy:`          
    `:ymmmmmmmmmmmmmmmm+       `ymmmmmmmy-`         .+hmmmmmmmmmmy/.     ./ymmmmmmmmmmh+.        
        `/ymmmmmmmmmmmmmm+       `ymmmmdy:`            ``:sdmmmmmmmmmho-`    `:sdmmmmmmmmmho-`     
        `:shmmmmmmmmmmm+       `ymmho-`                 `-oydmmmmmmmmdo`     `.+hmmmmmmmmmds:`   
            `.:oyhdmmmmmm+       `o+-`                       `.:/oosssso/`        .:oyhdmmmmmmd/   
                `.-:/+oss/        `                                                  `..-:::::-`   
                                                                                                """)
print('------------------------')
print ('Loading KillStream.me Bot')
try :
    with open("settings", 'rb') as settings_file:
        login_data = pickle.load(settings_file)
except :
    print ("Seems like you don't have a correct settings,\n please use the example provided to make one.")
    login_data = {}
    login_data["TOKEN"] = input("Please put in your bot Token: ")
    print ("okay saving this to new settings file")
    with open("settings", "wb") as settings_file:
        pickle.dump(login_data, settings_file)
        print ('Saved!')
client = discord.Client()

@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return
    # check if the server has settings
    if message.server.id not in login_data:
        with open("settings", "wb") as settings_file:
            login_data[message.server.id] = {}
            login_data[message.server.id]["TEXTCHANNEL"] = None
            login_data[message.server.id]["VOICECHANNEL"] = None
            login_data[message.server.id]["ALLOWGROUP"] = None
            pickle.dump(login_data, settings_file)
    # check if the user is a server admin or in the allowed group
    if not message.author.server_permissions.administrator and discord.utils.get(message.author.roles, name=login_data[message.server.id]["ALLOWGROUP"]) is None and login_data[message.server.id]["ALLOWGROUP"] != 'everyone':
        return
    # Check if you want to set or change the text channel, since this is said on any channel
    if message.content.startswith('!settextchannel'):
        if message.channel.is_private:
            return
        if 'reset' in message.content:
            login_data[message.server.id]["TEXTCHANNEL"] = None
            msg = 'I will listen everywhere'
        else:
            login_data[message.server.id]["TEXTCHANNEL"] = message.channel.name
            msg = 'I will only listen on {0.channel}'.format(message)
        with open("settings", "wb") as settings_file:
            pickle.dump(login_data, settings_file)
            msg +=' for commands now {0.author.mention}'.format(message)
            await client.send_message(message.channel, msg)
    # Check if the message is in the text channel
    if message.channel.name != login_data[message.server.id]["TEXTCHANNEL"] and login_data[message.server.id]["TEXTCHANNEL"] != None:
        return
    # Text Calls
    if message.content.startswith('!setvoicechannel'):
        if message.author.voice_channel != 'None':
            with open("settings", "wb") as settings_file:
                login_data[message.server.id]["VOICECHANNEL"] = message.author.voice_channel
                pickle.dump(login_data, settings_file)
                msg = 'Added to {0.author.voice_channel}, i will now play in here {0.author.mention}'.format(message)
                await client.send_message(message.channel, msg)

    if message.content.startswith('!setgroup '):
        with open("settings", "wb") as settings_file:
            login_data[message.server.id]["ALLOWGROUP"] = message.content[10:]
            pickle.dump(login_data, settings_file)
            msg = 'Group set to '+message.content[10:]+' {0.author.mention}'.format(message)
            await client.send_message(message.channel, msg)

    if message.content.startswith('!m3u8 '):
        response = urllib.request.urlopen(message.content[6:])
        html = response.read()
        start = 'hlsmp4":{"href":"'
        end = '.m3u8'
        if start not in str(html):
            msg = ('Could not find m3u8 for the link!').format(message)
            await client.send_message(message.channel, msg)
            return
        msg = ('Url: '+(str(html).split(start))[1].split(end)[0]+'.m3u8').format(message)
        await client.send_message(message.channel, msg)

    if message.content.startswith('!stream.me '):
        response = urllib.request.urlopen('https://www.stream.me/'+message.content[11:])
        html = response.read()
        start = 'hlsmp4":{"href":"'
        end = '.m3u8'
        if login_data[message.server.id]["VOICECHANNEL"] == None:
            msg = ('Sorry you need to set a room up for me to stream in. Just go into the room and type in !setvoicechannel in the chat.').format(message)
            await client.send_message(message.channel, msg)
            return
        if start not in str(html):
            return
        msg = ('Playing https://www.stream.me/'+message.content[11:]+'\nm3u8: '+str(html).split(start)[1].split(end)[0]+'.m3u8').format(message)
        await client.send_message(message.channel, msg)
        voice = client.voice_client_in(message.server)
        if voice != None:
            await voice.disconnect()
        voice = await client.join_voice_channel(login_data[message.server.id]["VOICECHANNEL"])
        player = voice.create_ffmpeg_player((str(html).split(start))[1].split(end)[0]+'.m3u8', options='-map p:1 -vn -loglevel quiet')
        player.start()
        while not player.is_done():
            await asyncio.sleep(1)
        await voice.disconnect()

    if message.content.startswith('!stop'):
        voice = client.voice_client_in(message.server)
        if voice != None:
            await voice.disconnect()
    
    if message.content.startswith('!help'):
        msg =   '```The KillStream.me Bot\n'
        msg +=  '----------\n'
        msg +=  'Current Commands\n'
        msg +=  '----------\n'
        msg +=  '!help                      : Gives you this help screen with all the fancy commands\n'
        msg +=  '!setvoicechannel           : Sets the voice channel that I go into to play the stream, The Current is: '+str(login_data[message.server.id]["VOICECHANNEL"])+'\n'
        msg +=  '!settextchannel            : Sets the channel that the bot listens to, The Current is: '+str(login_data[message.server.id]["TEXTCHANNEL"])+'\n'
        msg +=  '!setgroup <group name>     : Lets you set a group who can use the bot, The Current is: '+str(login_data[message.server.id]["ALLOWGROUP"])+'\n'
        msg +=  '!stream.me <stream name>   : Starts up a stream.me stream\n'
        msg +=  '!stop                      : Stops the stream\n'
        msg +=  '!m3u8 <url>                : gets the m3u8 url from a link (if it exists)\n'
        msg +=  '----------\n```'.format(message)
        await client.send_message(message.channel, msg)
@client.event


async def on_ready():
    print('Logged In')
    print('Dont forget to add this to your discords! Should work on multiple instances as well. (well ive tested a few at once, cant promise anything since the file storage is jank as fuck')


client.run(login_data["TOKEN"])