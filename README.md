# Killstream.me Discord Bot #

This is a discord bot based on python3.6 that will parse stream.me streams. Made specifically for the killstream and it not late and gay but it works for any stream.me channels.

### Get It Now ###

Want to try it out? [Click Here](https://discordapp.com/oauth2/authorize?client_id=524812752133554198&scope=bot) and try out the one i am hosting.
NOTE: This is running on a toaster VPS so if it isnt working or being sporatic just letting you know now.

### Setup ###

* Download Ubuntu Server, since i did it on there
* Install Python3.6 (or higher i guess this is just what i used)
* Install the rest with this snippet, this is just what i had to install to make sure things are working, let me know if I am missing something.
```
add-apt-repository ppa:deadsnakes/ppa
apt-get update
apt-get install python3.6 python3.6-dev
apt-get install build-essential libpq-dev python-dev libxml2-dev libxslt1-dev libldap2-dev libsasl2-dev libffi-dev git screen wget libopus0 ffmpeg
python3.6 -m pip install discord.py
python3.6 -m pip install asyncio
python3.6 -m pip install PyNaCl
cd ~
mkdir discord-killstream-me
cd discord-killstream-me
wget https://bitbucket.org/Vargink/killstream.me-discord-player/raw/master/discord-bot.py
screen
python3.6 ./discord-bot.py
```
after this press `Ctrl` + `a` + `d` to get the screen to disconnect, to reconnect to it run `screen -r` 
Might be more but this is all i can remember.

### Getting Started ###

Once you have added the bot to your discord all you need to do is enter a voice chat room and use the `!setvoicechannel` command to make the bot stream to that room.
After that all you need to do is use `!stream.me (channel name)` Like for example `!stream.me theralphretort`

### Commands ###

!help                      : Gives you this help screen with all the fancy commands
!setvoicechannel           : Sets the voice channel that I go into to play the stream
!settextchannel            : Sets the channel that the bot listens to
!setgroup (group name)     : Lets you set a group who can use the bot eg, `!setgroup everyone`, `!setgroup Admin`
!stream.me (stream name)   : Starts up a stream.me stream eg, `!stream.me theralphretort`
!stop                      : Stops the stream

### Who do I talk to? ###

* [Vargink](https://keybase.io/theguywho)